#include <stdio.h>
#include <malloc.h>
#include "set/unordered_array_set.h"
#include "set/ordered_array_set.h"
#include "set/bool_set.h"

void solveFirst() {
    UnorderedSet universe = {(int[]) {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, 10};
    UnorderedSet setA = {(int[]) {1, 2, 3, 8}, 4};
    UnorderedSet setB = {(int[]) {3, 6, 7}, 3};
    UnorderedSet setC = {(int[]) {2, 3, 4, 5, 7}, 5};

    UnorderedSet one = {(int[]){0, 0, 0}, 3};
    differenceU(setC, setA, &one);

    UnorderedSet two = {(int[]){0}, 1};
    differenceU(setB, setC, &two);

    UnorderedSet three = {(int[]){0}, 1};
    intersectionU(setB, setA, &three);

    UnorderedSet four;
    intersectionU(setA, two, &four);

    UnorderedSet five;
    differenceU(four, three, &five);

    UnorderedSet six = {(int[]){0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 10};
    addToUniverseU(universe, five, &six);

    UnorderedSet result = {(int[]){0, 0, 0, 0, 0, 0, 0}, 7};
    xorU(one, six, &result);

    printf("Task 1: ");
    printSetU(stdout, result);
}

void solveSecond() {
    OrderedSet setA = {(int[]) {1, 2, 3, 4, 5, 6, 7}, 7};
    OrderedSet setB = {(int[]) {2, 5, 6, 9, 10}, 5};
    OrderedSet setC = {(int[]) {4, 7, 8, 11, 12}, 5};

    OrderedSet one = {(int[]) {0, 0, 0, 0}, 4};
    differenceO(setA, setB, &one);

    OrderedSet two = {(int[]) {0, 0}, 2};
    differenceO(one, setC, &two);

    OrderedSet three = {(int[]) {0, 0}, 2};
    differenceO(setB, setA, &three);

    OrderedSet four = {(int[]) {0, 0, 0}, 3};
    differenceO(setC, setA, &four);

    OrderedSet five = {(int[]) {0, 0, 0, 0}, 4};
    unionO(two, three, &five);

    OrderedSet result = {(int[]) {0, 0, 0, 0, 0, 0, 0}, 7};
    unionO(four, five, &result);

    printf("Task 2: ");
    printSetO(stdout, result);
}

int main() {
    solveFirst();
    solveSecond();
}