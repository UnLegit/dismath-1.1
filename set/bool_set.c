#include "bool_set.h"
#include <minmax.h>
#include <malloc.h>

static bool isValidIndex(size_t setSize, int index) {
    return index >= 0 && index < setSize;
}

BoolSet createBoolSet(size_t size) {
    return (BoolSet) {
        size,
        (bool*) calloc(size, sizeof(bool))
    };
}

bool get(BoolSet set, int value) {
    return isValidIndex(set.size, value) && set.data[value];
}

void set(BoolSet* set, int index, bool value) {
    if (isValidIndex(set->size, index)) {
        set->data[index] = value;
    }
}

void printSetB(FILE *out, BoolSet set) {
    fprintf(out, "{");

    bool foundOneAtLeast = false;

    for (int i = 0; i < set.size; i++) {
        if (set.data[i]) {
            if (foundOneAtLeast) {
                fprintf(out, ", ");
            } else {
                foundOneAtLeast = true;
            }

            fprintf(out, "%d", i);
        }
    }

    fprintf(out, "}\n");
}

BoolSet unionB(BoolSet set1, BoolSet set2) {
    size_t newSize = max(set1.size, set2.size);
    BoolSet resultSet = createBoolSet(newSize);

    for (int i = 0; i < newSize; ++i) {
        if (get(set1, i) || get(set2, i)) {
            set(&resultSet, i, true);
        }
    }

    return resultSet;
}

BoolSet intersectionB(BoolSet set1, BoolSet set2) {
    size_t newSize = max(set1.size, set2.size);
    BoolSet resultSet = createBoolSet(newSize);

    for (int i = 0; i < newSize; ++i) {
        if (get(set1, i) && get(set2, i)) {
            set(&resultSet, i, true);
        }
    }

    return resultSet;
}

BoolSet differenceB(BoolSet set1, BoolSet set2) {
    size_t newSize = max(set1.size, set2.size);
    BoolSet resultSet = createBoolSet(newSize);

    for (int i = 0; i < newSize; ++i) {
        if (get(set1, i) && !get(set2, i)) {
            set(&resultSet, i, true);
        }
    }

    return resultSet;
}

BoolSet xorB(BoolSet set1, BoolSet set2) {
    size_t newSize = max(set1.size, set2.size);
    BoolSet resultSet = createBoolSet(newSize);

    for (int i = 0; i < newSize; ++i) {
        if (get(set1, i) != get(set2, i)) {
            set(&resultSet, i, true);
        }
    }

    return resultSet;
}

bool isSubB(BoolSet set, BoolSet checkCandidate) {
    if (checkCandidate.size > set.size) {
        return false;
    }

    for (int i = 0; i < set.size; ++i) {
        if (get(checkCandidate, i) && !get(set, i)) {
            return false;
        }
    }

    return true;
}

BoolSet addToUniverseB(BoolSet universe, BoolSet set) {
    return differenceB(universe, set);
}