#ifndef DISMATH_1_1_BOOL_SET_H
#define DISMATH_1_1_BOOL_SET_H

#include <stdbool.h>
#include <memory.h>
#include <stdio.h>

typedef struct BoolSet {
    size_t size;
    bool* data;
} BoolSet;

BoolSet createBoolSet(size_t size);

bool get(BoolSet set, int value);

void set(BoolSet* set, int index, bool value);

void printSetB(FILE* out, BoolSet set);

BoolSet unionB(BoolSet set1, BoolSet set2);

BoolSet intersectionB(BoolSet set1, BoolSet set2);

BoolSet differenceB(BoolSet set1, BoolSet set2);

BoolSet xorB(BoolSet set1, BoolSet set2);

bool isSubB(BoolSet set1, BoolSet checkCandidate);

BoolSet addToUniverseB(BoolSet universe, BoolSet set);

#endif //DISMATH_1_1_BOOL_SET_H