#include "ordered_array_set.h"

bool isFoundO(OrderedSet set, int value) {
    for (size_t i = 0; i < set.size; i++) {
        if (set.data[i] == value) {
            return true;
        }
    }
    return false;
}

static void pushback(OrderedSet* set, const int value) {
    set->data[set->size] = value;
    set->size++;
}

void printSetO(FILE* out, OrderedSet set) {
    fprintf(out, "{");

    for (size_t i = 0; i < set.size; i++) {
        fprintf(out, "%d", set.data[i]);

        if (i != (set.size - 1)) {
            fprintf(out, ", ");
        }
    }

    fprintf(out, "}\n");
}

void unionO(const OrderedSet set1, const OrderedSet set2, OrderedSet* outputSet) {
    outputSet->size = 0;

    size_t set1I = 0, set2I = 0;

    while (set1I < set1.size && set2I < set2.size) {
        if (set1.data[set1I] < set2.data[set2I]) {
            pushback(outputSet, set1.data[set1I++]);
        } else if (set1.data[set1I] == set2.data[set2I]) {
            pushback(outputSet, set1.data[set1I++]);
            set2I++;
        } else {
            pushback(outputSet, set2.data[set2I++]);
        }
    }

    while (set1I < set1.size) {
        pushback(outputSet, set1.data[set1I++]);
    }

    while (set2I < set2.size) {
        pushback(outputSet, set2.data[set2I++]);
    }
}

void intersectionO(const OrderedSet set1, const OrderedSet set2, OrderedSet* outputSet) {
    outputSet->size = 0;

    size_t set1I = 0, set2I = 0;

    while (set1I < set1.size && set2I < set2.size) {
        if (set1.data[set1I] == set2.data[set2I]) {
            pushback(outputSet, set1.data[set1I++]);
            set2I++;
        } else if (set1.data[set1I] < set2.data[set2I]) {
            set1I++;
        } else {
            set2I++;
        }
    }
}

void differenceO(const OrderedSet set1, const OrderedSet set2, OrderedSet* outputSet) {
    outputSet->size = 0;

    size_t set1I = 0, set2I = 0;

    while (set1I < set1.size && set2I < set2.size) {
        if (set1.data[set1I] == set2.data[set2I]) {
            set1I++;
            set2I++;
        } else if (set1.data[set1I] < set2.data[set2I]) {
            pushback(outputSet, set1.data[set1I++]);
        } else {
            set2I++;
        }
    }

    while (set1I < set1.size) {
        pushback(outputSet, set1.data[set1I++]);
    }
}

void xorO(const OrderedSet set1, const OrderedSet set2, OrderedSet* outSet) {
    outSet->size = 0;

    size_t set1I = 0, set2I = 0;

    while (set1I < set1.size && set2I < set2.size) {
        if (set1.data[set1I] < set2.data[set2I]) {
            pushback(outSet, set1.data[set1I++]);
        } else if (set1I == set1.size || set1.data[set1I] > set2.data[set2I]) {
            pushback(outSet, set2.data[set2I++]);
        } else {
            set1I++;
            set2I++;
        }
    }

    while (set1I < set1.size) {
        pushback(outSet, set1.data[set1I++]);
    }

    while (set2I < set2.size) {
        pushback(outSet, set2.data[set2I++]);
    }
}

bool isSubO(const OrderedSet set1, const OrderedSet set2) {
    size_t equalCounter = 0, set1I = 0, set2I = 0;

    while (set1I < set1.size && set2I < set2.size) {
        if (set1.data[set1I] == set2.data[set2I]) {
            set1I++;
            set2I++;
            equalCounter++;
        } else if (set1.data[set1I] < set2.data[set2I]) {
            set1I++;
        } else {
            set2I++;
        }
    }

    return equalCounter == set1.size;
}
bool areEqualO(const OrderedSet set1, const OrderedSet set2) {
    if (set1.size != set2.size) {
        return false;
    }

    return isSubO(set1, set2);
}

void addToUniverseO(OrderedSet universe, OrderedSet set, OrderedSet* outputSet) {
    differenceO(universe, set, outputSet);
}