#ifndef DISMATH_1_1_ORDERED_ARRAY_SET_H
#define DISMATH_1_1_ORDERED_ARRAY_SET_H

#include <stdio.h>
#include <stdbool.h>

typedef struct OrderedSet {
    int* data;
    size_t size;
} OrderedSet;

bool isFoundO(OrderedSet set, int value);

void printSetO(FILE* out, OrderedSet set);

void unionO(OrderedSet set1, OrderedSet set2, OrderedSet* outputSet);

void intersectionO(OrderedSet set1, OrderedSet set2, OrderedSet* outputSet);

void differenceO(OrderedSet set1, OrderedSet set2, OrderedSet* outputSet);

void xorO(OrderedSet set1, OrderedSet set2, OrderedSet* outSet);

bool isSubO(OrderedSet set1, OrderedSet set2);

bool areEqualO(OrderedSet set1, OrderedSet set2);

void addToUniverseO(OrderedSet universe, OrderedSet set, OrderedSet* outputSet);

#endif //DISMATH_1_1_ORDERED_ARRAY_SET_H