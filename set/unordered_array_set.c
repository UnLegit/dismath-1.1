#include "unordered_array_set.h"
#include <memory.h>

bool isFoundU(UnorderedSet set, int value) {
    for (size_t i = 0; i < set.size; i++) {
        if (set.data[i] == value) {
            return true;
        }
    }
    return false;
}

static void pushback(UnorderedSet* set, const int value) {
    set->data[set->size] = value;
    set->size++;
}

void printSetU(FILE* out, UnorderedSet set) {
    fprintf(out, "{");

    for (size_t i = 0; i < set.size; i++) {
        fprintf(out, "%d", set.data[i]);

        if (i != (set.size - 1)) {
            fprintf(out, ", ");
        }
    }

    fprintf(out, "}\n");
}

void unionU(const UnorderedSet set1, const UnorderedSet set2, UnorderedSet* outputSet) {
    memcpy(outputSet->data, set1.data, set1.size * sizeof(int));

    outputSet->size = set1.size;

    for (size_t i = 0; i < set2.size; i++) {
        if (!isFoundU(set1, set2.data[i])) {
            pushback(outputSet, set2.data[i]);
        }
    }
}

void intersectionU(const UnorderedSet set1, const UnorderedSet set2, UnorderedSet* outputSet) {
    outputSet->size = 0;

    for (size_t i = 0; i < set2.size; i++) {
        if (isFoundU(set1, set2.data[i])) {
            pushback(outputSet, set2.data[i]);
        }
    }
}

void differenceU(const UnorderedSet set1, const UnorderedSet set2, UnorderedSet* outputSet) {
    outputSet->size = 0;

    for (size_t i = 0; i < set1.size; i++) {
        if (!isFoundU(set2, set1.data[i])) {
            pushback(outputSet, set1.data[i]);
        }
    }
}

void xorU(const UnorderedSet set1, const UnorderedSet set2, UnorderedSet* outputSet) {
    outputSet->size = 0;

    for (size_t i = 0; i < set1.size; i++) {
        if (!isFoundU(set2, set1.data[i])) {
            pushback(outputSet, set1.data[i]);
        }
    }

    for (size_t i = 0; i < set2.size; i++) {
        if (!isFoundU(set1, set2.data[i])) {
            pushback(outputSet, set2.data[i]);
        }
    }
}

bool isSubU(const UnorderedSet set1, const UnorderedSet set2) {
    for (size_t i = 0; i < set1.size; i++) {
        if (!isFoundU(set2, set1.data[i])) {
            return false;
        }
    }
    return true;
}

bool areEqualU(const UnorderedSet set1, const UnorderedSet set2) {
    return (set1.size == set2.size) ? isSubU(set1, set2) : false;
}

void addToUniverseU(const UnorderedSet universe, const UnorderedSet set, UnorderedSet* outputSet) {
    differenceU(universe, set, outputSet);
}