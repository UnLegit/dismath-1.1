#ifndef DISMATH_1_1_UNORDERED_ARRAY_SET_H
#define DISMATH_1_1_UNORDERED_ARRAY_SET_H

#include <stdio.h>
#include <stdbool.h>

typedef struct UnorderedSet {
    int *data;
    size_t size;
} UnorderedSet;

bool isFoundU(UnorderedSet set, int value);

void printSetU(FILE *out, UnorderedSet set);

void unionU(UnorderedSet set1, UnorderedSet set2, UnorderedSet *outputSet);

void intersectionU(UnorderedSet set1, UnorderedSet set2, UnorderedSet *outputSet);

void differenceU(UnorderedSet set1, UnorderedSet set2, UnorderedSet *outputSet);

void xorU(UnorderedSet set1, UnorderedSet set2, UnorderedSet *outputSet);

bool isSubU(UnorderedSet set1, UnorderedSet set2);

bool areEqualU(UnorderedSet set1, UnorderedSet set2);

void addToUniverseU(UnorderedSet universe, UnorderedSet set, UnorderedSet *outputSet);

#endif //DISMATH_1_1_UNORDERED_ARRAY_SET_H
